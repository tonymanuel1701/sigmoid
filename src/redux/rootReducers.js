import dashboardReducer from "./dashboard/reducer";
import loginReducer from "./login/reducer";

const rootReducers = {
  loginReducer: loginReducer,
  dashboardReducer: dashboardReducer,
};

export default rootReducers;
