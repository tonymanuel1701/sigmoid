import { loginAction } from "../../api/login";
import { DASHBOARD } from "../../constants/routes";
import {
  RESET_LOGIN_REDUCER,
  SET_AUTH_STATUS,
  SET_IS_FETCHING,
  SET_LOGIN_DETAILS,
  SET_TOKEN,
} from "./actionType";

export const setCredientials = (key, value) => {
  return { type: SET_LOGIN_DETAILS, key, value };
};

export const setAuthStatus = (response) => {
  return { type: SET_AUTH_STATUS, response };
};
export const setIsFetching = (status) => {
  return { type: SET_IS_FETCHING, status: status };
};

export const setToken = (token) => {
  return { type: SET_TOKEN, token };
};

export const resetLoginReducer = () => {
  return { type: RESET_LOGIN_REDUCER };
};

export const loginUser = (payload, history) => {
  return (dispatch) => {
    dispatch(setIsFetching(true));
    dispatch(setAuthStatus(""));
    loginAction(payload)
      .then((res) => {
        dispatch(setIsFetching(false));
        dispatch(setToken(res.data.token));
        localStorage.setItem("sigToken", res.data.token);
        history.replace(DASHBOARD);
      })
      .catch((err) => {
        dispatch(setIsFetching(false));
        dispatch(setAuthStatus(err.response.data));
      });
  };
};
