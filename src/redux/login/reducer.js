import {
  SET_AUTH_STATUS,
  SET_LOGIN_DETAILS,
  SET_IS_FETCHING,
  SET_TOKEN,
  RESET_LOGIN_REDUCER,
} from "./actionType";

const initialState = {
  emailId: { isValid: true, value: "" },
  password: { isValid: true, value: "" },
  rememberMe: false,
  isFetching: false,
  authResponse: "",
  isAuthenticated: false,
  token: "",
};

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_TOKEN:
      return { ...state, isAuthenticated: action.token };
    case SET_LOGIN_DETAILS:
      return { ...state, [action.key]: action.value };
    case SET_AUTH_STATUS:
      return { ...state, authResponse: action.response };
    case SET_IS_FETCHING:
      return { ...state, isFetching: action.status };

    case RESET_LOGIN_REDUCER:
      return {
        ...state,
        emailId: { isValid: true, value: "" },
        password: { isValid: true, value: "" },
        rememberMe: false,
        isFetching: false,
        authResponse: "",
      };
    default:
      return state;
  }
};

export default loginReducer;
