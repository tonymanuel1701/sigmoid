import { combineReducers, createStore, applyMiddleware, compose } from "redux";
import ReduxThunk from "redux-thunk";
import reducers from "./rootReducers";

//to enable redux dev tools
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducers = combineReducers(reducers);

const store = createStore(
  rootReducers,
  composeEnhancers(applyMiddleware(ReduxThunk))
);

export default store;
