import {
  SET_DATE_RANGE,
  SET_TO_MIN_DATE,
  SET_ALLOWED_DATES,
  SET_CHART_DATA,
  SET_CHART_LOADING,
  RESET_DASHBOARD,
} from "./actionType";

const initialState = {
  startDate: 0,
  endDate: 0,
  toMinDate: 0,
  allowedDates: { startDate: new Date(), endDate: new Date() },
  chart1: [],
  chart2: [],
  chart3: [],
  chart1Loading: true,
  chart2Loading: true,
  chart3Loading: true,
};

const dashboardReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_DATE_RANGE:
      return { ...state, [action.field]: action.date };
    case SET_TO_MIN_DATE:
      return { ...state, toMinDate: action.date };
    case SET_ALLOWED_DATES:
      return { ...state, allowedDates: action.date };
    case SET_CHART_DATA:
      return { ...state, [action.target]: action.data };
    case SET_CHART_LOADING:
      return { ...state, [action.target]: action.status };

    case RESET_DASHBOARD:
      return {
        ...state,
        startDate: 0,
        endDate: 0,
        toMinDate: 0,
        allowedDates: { startDate: new Date(), endDate: new Date() },
        chart1: [],
        chart2: [],
        chart3: [],
        chart1Loading: true,
        chart2Loading: true,
        chart3Loading: true,
      };
    default:
      return state;
  }
};

export default dashboardReducer;
