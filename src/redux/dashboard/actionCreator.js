import { getChartData, getDateRange } from "../../api/dashboard";

import {
  SET_DATE_RANGE,
  SET_TO_MIN_DATE,
  SET_ALLOWED_DATES,
  SET_CHART_DATA,
  SET_CHART_LOADING,
  RESET_DASHBOARD,
} from "./actionType";

/**
 * @param {*determine which date picker is sending the value. Is it fromDate or toDate } field
 * @param {* unix millies} date
 */
export const setDate = (field, date) => {
  return { type: SET_DATE_RANGE, field, date };
};

/**
 * @param {* sets the mindate of dateTo according to startDate picker } date
 */
export const setTo_MinDate = (date) => {
  return { type: SET_TO_MIN_DATE, date };
};

/**
 * @param {* sets the date range recieved from the api} date
 * @returns
 */
export const setAllowedDates = (date) => {
  return { type: SET_ALLOWED_DATES, date };
};

/**
 *
 * @param {*callback function for route redirects} history
 * @param {*route redirection path } path
 * @param {*callback function to initial chart reload} reloadChart
 */
export const setDateRange = (history, path, reloadChart) => {
  return (dispatch) => {
    let payload = { organization: "DemoTest", view: "Auction" };
    getDateRange(payload)
      .then((res) => {
        dispatch(setAllowedDates(res.data.result));
        dispatch(setDate("startDate", res.data.result.startDate));
        dispatch(setDate("endDate", res.data.result.endDate));
        //callback to reload the initial charts
        reloadChart();
      })
      .catch((err) => {
        err.response.status == 401 && history.replace(path);
      });
  };
};

/**
 * @param {*determine which chart type} target
 * @param {* chart data from api} data
 */
export const setChartData = (target, data = []) => {
  return { type: SET_CHART_DATA, target, data };
};

/**
 * @param {*determine which chart } target
 * @param {* boolean for toggling spinner} status
 */
export const setChartLoading = (target, status) => {
  return { type: SET_CHART_LOADING, target, status };
};

/**
 * @param {*determine which chart type} target
 * @param {*payload for each chart type} payload
 * @returns
 */
export const loadCharts = (target, payload) => {
  return (dispatch) => {
    dispatch(setChartData(target, []));
    dispatch(setChartLoading(target + "Loading", true));
    getChartData(payload)
      .then((res) => {
        dispatch(setChartData(target, res.data.result.data));
        dispatch(setChartLoading(target + "Loading", false));
      })
      .catch((err) => {
        dispatch(setChartLoading(target + "Loading", false));
      });
  };
};

export const resetDashBoardReducer = () => {
  return { type: RESET_DASHBOARD };
};
