import { DASHBOARD, LOGOUT } from "../constants/routes";
import Dashboard from "../pages/Dashboard";
import Auth from "../pages/Login";

const routes = [
  {
    path: DASHBOARD,
    component: Dashboard,
  },
  { path: LOGOUT, component: Auth },
];

export default routes;
