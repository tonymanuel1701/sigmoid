export const API_SIGNIN = "/signIn";
export const API_GET_DATE_RANGE = "/getDateRange";
export const API_GET_DATA = "/getData";
