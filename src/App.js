import { BrowserRouter, Redirect, Route } from "react-router-dom";
import Protected from "./components/hoc/Protected";
import { AUTH } from "./constants/routes";
import routes from "./json/routes";
import Auth from "./pages/Login";

function App() {
  return (
    <BrowserRouter>
      <Route exact path={AUTH} component={Auth} />
      {routes.map((route, index) => (
        <Protected key={index} component={route.component} path={route.path} />
      ))}
      <Redirect to={AUTH} />
    </BrowserRouter>
  );
}

export default App;
