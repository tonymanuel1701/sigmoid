import axios from "../axios-interceptor/axios";

export const postCall = async (URL, data) => axios.post(URL, data);
