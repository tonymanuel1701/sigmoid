import axios from "axios";

const instance = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
});

instance.interceptors.request.use(
  (req) => {
    const token = localStorage.getItem("sigToken");

    if (token) {
      req.headers["X-Auth-Token"] = token;
    }
    return req;
  },
  (error) => {
    console.log(error);
  }
);
instance.interceptors.response.use((res) => res);

export default instance;
