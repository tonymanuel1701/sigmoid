import validator from "validator";

export const validate = (key, value) => {
  let isValid = false;
  switch (key) {
    case "emailId":
      isValid = validator.isEmail(value);
      break;
    case "password":
      isValid = !validator.isEmpty(value);
      break;
    default:
      isValid = false;
  }
  return { isValid: isValid, value: value };
};
