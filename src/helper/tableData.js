import moment from "moment";
import CanvasJSReact from "../assets/canvasJs/canvasjs.react";

/**
 *
 * @param {array of objects} data
 * function returns the modified data along with seperate columns heading as an object
 * Columns headings is used for showing in table head
 * @returns
 */
export const genericTableStructure = (data = []) => {
  const columnHeadings = getColumnHeadings(data?.length > 0 ? data[0] : []);
  return { columnHeadings: columnHeadings, rows: data };
};

const getColumnHeadings = (data) => {
  return Object.keys(data);
};

export const getChartDataPoints = (data = []) => {
  if (data.length < 1) return data;

  let dataPoints = data.map((item) => {
    return { label: item.advertiserId, y: item.CM001_percent, z: item.CM001 };
  });

  let pieChartData = {
    data: [
      {
        type: "pie",
        startAngle: 75,
        showInLegend: "true",
        indexLabelFontSize: 16,
        toolTipContent: "<b>{label}</b>: {z}",
        legendText: "{label}",
        indexLabel: "{label} - {y}%",
        dataPoints: dataPoints,
      },
    ],
  };

  return pieChartData;
};

export const lineChartDataPoints = (data = []) => {
  const byDate = data.slice(0);
  byDate.sort(function (a, b) {
    return a.day - b.day;
  });

  let newDataPoints = [];
  byDate.map((item) => {
    const dateObj = moment(+item.day);

    let day = dateObj.date();
    let month = dateObj.month();
    let year = dateObj.year();

    newDataPoints.push({
      x: new Date(year, month, day),
      y: +item.impressions_offered,
    });
  });

  const lineChart = {
    axisY: {
      title: "Impressions Offered",
      suffix: "",
    },
    axisX: {
      title: "Day",
      labelAngle: -50,
    },
    data: [
      {
        type: "line",
        toolTipContent: "Day {x}: {y}",
        dataPoints: newDataPoints,
      },
    ],
  };
  console.log(lineChart);

  return lineChart;
};
