import Navbar from "../components/navbar/Navbar";
import Wrapper from "../components/page-utils/Wrapper";
import DateRangePicker from "../components/dashboard/DateRangePicker";
import DashboardReports from "../components/dashboard/DashboardReports";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { resetDashBoardReducer } from "../redux/dashboard/actionCreator";
const wrapperClass = `flex flex-col flex-grow items-center  overflow-auto h-screen `;
const contentClass = " bg-primary  w-full text-white";

const PageContent = ({ children }) => (
  <Wrapper className={contentClass}>{children}</Wrapper>
);

const Dashboard = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    return () => {
      dispatch(resetDashBoardReducer());
    };
  });
  return (
    <Wrapper className={wrapperClass}>
      <Navbar />
      <PageContent>
        <DateRangePicker />
        <DashboardReports />
      </PageContent>
    </Wrapper>
  );
};

export default Dashboard;
