import React from "react";
import { Typography, Box } from "@material-ui/core";
import Wrapper from "../components/page-utils/Wrapper";
import Login from "../components/authentication/Login";

const wrapperClass = "flex bg-primary h-screen ";
const LeftColStyles = `flex flex-col flex-grow-0 items-center text-white p-16 text-center md:p-32 md:items-start md:flex-shrink-0 md:flex-1 md:text-left`;
const RightColStyles = `flex flex-col items-center justify-center p-4 m-4 md:m-0 md:p-12 md:pt-128 max-w-md max-w-sm min-w-sm min-w-md   bg-white`;

const SubHeadingStyles = "font-light mt-5 max-w-3xl";

const LeftCol = ({ children }) => (
  <Box className={LeftColStyles}>{children}</Box>
);

const RightCol = ({ children }) => (
  <Box className={RightColStyles}>{children}</Box>
);

const Heading = ({ children }) => (
  <Typography variant="h3" className="font-light">
    {children}
  </Typography>
);

const SubHeading = ({ children }) => (
  <Typography variant="subtitle1" className={SubHeadingStyles}>
    {children}
  </Typography>
);

const Auth = ({
  heading = "<UI/> Assignment",
  subHeading = "Discovering data trends and patterns to derive real-time insights by leveraging expertise across industry sectors to solve business challenges.",
}) => {
  return (
    <Wrapper className={wrapperClass}>
      <LeftCol>
        <Heading>{heading}</Heading>
        <SubHeading>{subHeading}</SubHeading>
      </LeftCol>
      <RightCol>
        <Login />
      </RightCol>
    </Wrapper>
  );
};

export default Auth;
