import { postCall } from "../axios-interceptor/apiCaller";
import { API_GET_DATA, API_GET_DATE_RANGE } from "../constants/apiEndPoints";

export const getDateRange = async (payload) => {
  return postCall(API_GET_DATE_RANGE, payload);
};

export const getChartData = async (payload) => {
  return postCall(API_GET_DATA, payload);
};
