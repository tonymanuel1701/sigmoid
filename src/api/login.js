import axios from "axios";
import { API_SIGNIN } from "../constants/apiEndPoints";

export const loginAction = async (credentials) => {
  return axios.post(process.env.REACT_APP_API_AUTH + API_SIGNIN, credentials);
};
