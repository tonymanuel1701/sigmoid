const { createMuiTheme } = require("@material-ui/core");
const theme = createMuiTheme({
  typography: {
    fontFamily: [
      "Ubuntu",
      "Montserrat",
      "Roboto",
      "Helvetica",
      "Arial",
      "sans-serif",
    ].join(","),
  },
  palette: {
    primary: { main: "#122230" },
    secondary: { main: "#18ffff" },
  },
  props: {
    MuiPaper: {
      variant: "outlined",
    },
    MuiButton: {
      variant: "contained",
      size: "small",
      color: "primary",
      disableElevation: true,
    },
    MuiTextField: {
      variant: "outlined",
      fullWidth: true,
    },
  },
});
export default theme;
