import { Button, Typography } from "@material-ui/core";
import { withRouter } from "react-router";
import { AUTH } from "../../constants/routes";
import Wrapper from "../page-utils/Wrapper";
const wrapperClass = "flex  w-full justify-between p-5 border-b  ";
const Navbar = (props) => {
  const logoutHandler = () => {
    localStorage.removeItem("sigToken");
    props.history.replace(AUTH);
  };

  return (
    <Wrapper className={wrapperClass}>
      <Typography variant="h6">WELCOME</Typography>
      <Button onClick={logoutHandler}>Logout</Button>
    </Wrapper>
  );
};

export default withRouter(Navbar);
