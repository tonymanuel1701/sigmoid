import { Redirect, Route, withRouter } from "react-router-dom";
import { AUTH } from "../../constants/routes";
const Protected = ({ path, component: Component, ...rest }) => {
  const token = localStorage.getItem("sigToken");
  return (
    <Route
      {...rest}
      path={path}
      exact
      render={(props) => {
        return token ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: AUTH, state: { from: props.location } }} />
        );
      }}
    />
  );
};

export default withRouter(Protected);
