import { Typography } from "@material-ui/core";
import Center from "../UI/Center";

const NoRecords = ({ title = "No Records Found" }) => (
  <Center>
    <Typography variant="caption">{title}</Typography>
  </Center>
);

export default NoRecords;
