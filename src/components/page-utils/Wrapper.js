import { Box } from "@material-ui/core";

const Wrapper = ({ className = "", style = {}, children }) => (
  <Box className={className} style={style}>
    {children}
  </Box>
);

export default Wrapper;
