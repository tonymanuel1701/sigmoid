import { useSelector } from "react-redux";
import {
  genericTableStructure,
  getChartDataPoints,
  lineChartDataPoints,
} from "../../helper/tableData";
import Wrapper from "../page-utils/Wrapper";
import Panel from "../UI/Panel";
import PieChart from "../UI/PieChart";
import TableWithPagination from "../UI/TableWithPagination";

const wrapperClass = "grid grid-cols-2 gap-4 p-5";
const panelStyle = { minHeight: 500 };

const DashboardReports = ({
  chart1Title = "Chart 1",
  chart2Title = "Chart 2",
  chart3Title = "Chart 3",
}) => {
  const {
    chart1,
    chart2,
    chart3,
    chart1Loading,
    chart2Loading,
    chart3Loading,
  } = useSelector((state) => state.dashboardReducer);

  const table1Data = genericTableStructure([...chart1]);
  const table2Data = genericTableStructure([...chart2]);
  const pieChartData = getChartDataPoints([...chart3]);
  const lineChartData = lineChartDataPoints([...chart1]);

  return (
    <Wrapper className={wrapperClass}>
      <Panel title={chart1Title} style={panelStyle}>
        <TableWithPagination {...table1Data} isLoading={chart1Loading} />
      </Panel>
      <Panel title={chart2Title} style={panelStyle}>
        <TableWithPagination {...table2Data} isLoading={chart2Loading} />
      </Panel>
      <Panel title={chart3Title} style={panelStyle}>
        <PieChart params={pieChartData} isLoading={chart3Loading} />
      </Panel>
      <Panel title={chart3Title} style={panelStyle}>
        <PieChart params={lineChartData} isLoading={chart3Loading} />
      </Panel>
    </Wrapper>
  );
};
export default DashboardReports;
