import { Button } from "@material-ui/core";
import moment from "moment";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { AUTH } from "../../constants/routes";
import {
  chart1Payload,
  chart2Payload,
  chart3Payload,
} from "../../json/samplePayloads";
import {
  loadCharts,
  setDate,
  setDateRange,
  setTo_MinDate,
} from "../../redux/dashboard/actionCreator";
import Wrapper from "../page-utils/Wrapper";
import DateRange from "../UI/DateRange";

const wrapperClass = "flex bg-white p-5 w-1/2 space-x-5 mt-5 mx-auto";

const DateRangePicker = ({
  fieldStartDate = "startDate",
  fieldEndDate = "endDate",
  labelFrom = "From",
  labelTo = "To",
  btnLabel = "Show",
}) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const state = useSelector((state) => state.dashboardReducer);

  const setDateRangeHandler = (field, date) => {
    date = moment(date).valueOf();
    dispatch(setDate(field, date));
    if (field === fieldStartDate) {
      dispatch(setTo_MinDate(date));
      if (state.endDate && date > state.endDate) {
        dispatch(setDate(fieldEndDate, +state.allowedDates.endDate));
      }
    }
  };

  useEffect(() => {
    dispatch(setDateRange(history, AUTH, reloadCharts));
  }, []);

  const reloadCharts = () => {
    dispatch(loadCharts("chart1", chartParams(chart1Payload)));
    dispatch(loadCharts("chart2", chartParams(chart2Payload)));
    dispatch(loadCharts("chart3", chartParams(chart3Payload)));
  };

  /**
   * inserts the date range selected by the user into the chart api payload
   * For initial reload the the date range recieved from the api is used
   */
  const chartParams = (payload) => {
    const { startDate, endDate } = state;
    if (startDate) {
      let params = {
        ...payload,
        chartObject: {
          ...payload.chartObject,
          requestParam: {
            ...payload.chartObject.requestParam,
            dateRange: {
              startDate: startDate.toString(),
              endDate: endDate.toString(),
            },
          },
        },
      };
      return params;
    }
    return payload;
  };

  return (
    <Wrapper className={wrapperClass}>
      {/* From Date */}
      <DateRange
        startDate={state.allowedDates.startDate}
        endDate={state.allowedDates.endDate}
        inputLabel={labelFrom}
        selected={
          state.startDate ? state.startDate : state.allowedDates.startDate
        }
        handler={(date) => setDateRangeHandler(fieldStartDate, date)}
      />
      {/* toDate-picker
          startDate will the date chosen in "from date"
          endDate is the response from api
          selected- by default the end date from api will be use else the date selected
       */}
      <DateRange
        startDate={state.toMinDate}
        endDate={state.allowedDates.endDate}
        inputLabel={labelTo}
        selected={state.endDate ? state.endDate : state.allowedDates.endDate}
        handler={(date) => setDateRangeHandler(fieldEndDate, date)}
      />
      <Button onClick={reloadCharts}>{btnLabel}</Button>
    </Wrapper>
  );
};
export default DateRangePicker;
