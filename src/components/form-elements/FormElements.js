import {
  Checkbox,
  Typography,
  TextField,
  FormControlLabel,
  Button,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import Spinner from "../UI/Spinner";

export const Form = ({ children }) => (
  <form noValidate autoComplete="off" className="space-y-4">
    {children}
  </form>
);

export const Heading = ({ children }) => (
  <Typography variant="subtitle1">{children}</Typography>
);

export const SubHeading = ({ children }) => (
  <Typography variant="body2">{children}</Typography>
);

export const Name = ({
  value,
  error = false,
  helperText = "",
  handler = () => {},
}) => (
  <TextField
    required
    label="Name"
    error={error}
    helperText={helperText}
    value={value}
    onChange={handler}
  />
);

export const Email = ({
  value,
  isValid = true,
  helperText = "Please enter a valid email address",
  handler = () => {},
}) => (
  <TextField
    required
    label="Email"
    name="emailId"
    error={!isValid}
    helperText={!isValid && helperText}
    value={value}
    onChange={handler}
  />
);

export const Password = ({
  value,
  isValid = true,
  helperText = "Password cannot be empty",
  handler = () => {},
}) => (
  <TextField
    type="password"
    name="password"
    label="Password"
    error={!isValid}
    value={value}
    helperText={!isValid && helperText}
    onChange={handler}
  />
);

export const RememberMe = ({ value, handler = () => {} }) => (
  <FormControlLabel
    style={{ marginTop: 5 }}
    control={<Checkbox color="secondary" />}
    label="Remember Me"
    name="rememberMe"
    onChange={handler}
  />
);

export const LoginButton = ({ showSpinner = false, handler = () => {} }) => (
  <Button fullWidth size="medium" onClick={!showSpinner ? handler : () => {}}>
    {showSpinner ? <Spinner /> : "LOGIN"}
  </Button>
);

export const ForgotPassword = () => (
  <Link to="#">
    <Typography>Forgot Password</Typography>
  </Link>
);
