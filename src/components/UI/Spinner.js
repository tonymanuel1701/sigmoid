import React from "react";
import { HashLoader } from "react-spinners";

const Spinner = ({ size = 24 }) => {
  return <HashLoader size={size} color={"#36D7B7"} />;
};

export default Spinner;
