import CanvasJSReact from "../../assets/canvasJs/canvasjs.react";
import NoRecords from "../page-utils/NoRecords";
import Center from "./Center";
import Spinner from "./Spinner";
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

const PieChart = ({
  type = "",
  title = "",
  params = {},
  dataPoints = {},
  isLoading = false,
}) => {
  const options = {
    animationEnabled: true,
    title: {
      text: title,
    },
    ...params,
  };
  return isLoading ? (
    <Center>
      <Spinner size={50} />
    </Center>
  ) : Object.keys(params).length ? (
    <CanvasJSChart options={options} />
  ) : (
    <NoRecords />
  );
};
export default PieChart;
