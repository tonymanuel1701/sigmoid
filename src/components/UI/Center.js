import Wrapper from "../page-utils/Wrapper";

const wrapperClass = "absolute flex items-center justify-center h-full inset-0";
const Center = ({ children }) => (
  <Wrapper className={wrapperClass}>{children}</Wrapper>
);
export default Center;
