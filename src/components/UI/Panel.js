import React from "react";
import { Typography } from "@material-ui/core";
import Wrapper from "../page-utils/Wrapper";

const wrapperClass = `rounded-xl relative p-5 bg-gray-50 shadow-md border text-black`;

const titleClass = `mt-2 mb-2 text-black`;

const PanelTitle = ({ children }) => (
  <Wrapper className={titleClass}>
    <Typography variant="subtitle2">{children}</Typography>
  </Wrapper>
);

const PanelContent = ({ children }) => <Wrapper>{children}</Wrapper>;

const Panel = ({
  title = "Default Title",
  children,
  size,
  className = "",
  style,
}) => (
  <Wrapper className={wrapperClass + " " + className} style={style}>
    <PanelTitle>{title}</PanelTitle>
    <PanelContent>{children}</PanelContent>
  </Wrapper>
);

export default Panel;
