import MomentUtils from "@date-io/moment";
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import moment from "moment";
import React from "react";

const DateRange = ({
  startDate = false,
  endDate = false,
  inputLabel = "Default",
  dateFormat = "DD-MMM-y",
  views = ["year", "month", "date"],
  variant = "inline",
  inputVariant = "outlined",
  handler,
  selected = new Date(),
}) => {
  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <DatePicker
        size="small"
        views={views}
        minDate={moment(+startDate)}
        maxDate={moment(+endDate)}
        autoOk
        variant={variant}
        inputVariant={inputVariant}
        label={inputLabel}
        format={dateFormat}
        value={moment(+selected)}
        onChange={handler}
      />
    </MuiPickersUtilsProvider>
  );
};

export default DateRange;
