import { Alert } from "@material-ui/lab";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { validate } from "../../helper/validate";
import {
  loginUser,
  resetLoginReducer,
  setCredientials,
} from "../../redux/login/actionCreator";
import {
  Email,
  ForgotPassword,
  Form,
  Heading,
  LoginButton,
  Password,
  RememberMe,
} from "../form-elements/FormElements";
import Wrapper from "../page-utils/Wrapper";

const wrapperClass = "space-y-3";
const linksWrapper = "flex items-center justify-between m-0";

const Login = ({
  heading = "LOGIN TO YOUR ACCOUNT",
  fieldEmail = "emailId",
  fieldPassword = "password",
  fieldRemember = "rememberMe",
}) => {
  const login = useSelector((state) => state.loginReducer);
  const dispatch = useDispatch();
  const history = useHistory();

  /**captures the form input and sends to reducer */
  const setCredentialsHanlder = (e) => {
    const field = e.target;
    const value =
      field.name === fieldRemember
        ? field.checked
        : validate(field.name, field.value);
    dispatch(setCredientials(field.name, value));
  };

  /**
   * to validate user login inputs and show errors upon login button click
   */
  const checkEmailAndPassword = () => {
    const email = validate(fieldEmail, login.emailId.value);
    const pwd = validate(fieldPassword, login.password.value);
    !email.isValid && dispatch(setCredientials(fieldEmail, email));
    !pwd.isValid && dispatch(setCredientials(fieldPassword, pwd));
    return [email, pwd];
  };

  const loginHandler = () => {
    const [email, password] = checkEmailAndPassword();
    if (email.isValid && password.isValid) {
      const payload = {
        email: email.value,
        password: password.value,
        rememberMe: login.rememberMe,
      };
      dispatch(loginUser(payload, history));
    }
  };

  /**Clear the login reducer when this component is unmounted */
  useEffect(() => {
    localStorage.removeItem("sigToken");
    return () => {
      dispatch(resetLoginReducer());
    };
  }, []);

  return (
    <Wrapper className={wrapperClass}>
      <Heading>{heading}</Heading>
      <Form>
        <Email
          handler={setCredentialsHanlder}
          isValid={login.emailId.isValid}
          value={login.emailId.value}
        />
        <Password
          handler={setCredentialsHanlder}
          isValid={login.password.isValid}
          value={login.password.value}
        />
        <Wrapper className={linksWrapper}>
          <RememberMe handler={setCredentialsHanlder} />
          <ForgotPassword />
        </Wrapper>

        {login.authResponse.statusMessage && (
          <Alert severity="error"> {login.authResponse.statusMessage}</Alert>
        )}
        <LoginButton handler={loginHandler} showSpinner={login.isFetching} />
      </Form>
    </Wrapper>
  );
};
export default Login;
