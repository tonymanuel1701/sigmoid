module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    backgroundColor: (theme) => ({
      ...theme("colors"),
      primary: "#122230",
      secondary: "#ffed4a",
      "primary-2": "#192d3e",
      "semi-dark": "rgb(18,18,18)",
    }),
    gradientColorStops: (theme) => ({
      ...theme("colors"),
      "gr-primary": "#122230",
      secondary: "#ffed4a",
      "primary-2": "#192d3e",
      danger: "#e3342f",
    }),
    extend: {
      height: {
        almost: "calc(100vh - 4rem)",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
  corePlugins: {
    // ...
    ringWidth: false,
    fontFamily: false,
    outline: false,
  },
};
